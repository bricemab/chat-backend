const express = require('express');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const router = express.Router();
const GlobalStore = require('../utils/GlobalStore');
const Auth = require('../utils/Auth');
const genericSchema = require("../schemas/genericSchema");
const UserManager = require('../lib/UserManager');

router.post("/authentification", function (request, response) {
  const config = GlobalStore.get("config");

  // Joi.validate(request.body, )
  Joi.validate(request.body, genericSchema.apiUser, function (error, data) {
    if (error) {
      console.log(error);
      return response.send({
        success: false,
        code: "missing-fields",
        error: error
      })
    }

    UserManager.validateLoginAndPassword(data.loginOrEmail, data.password).then((dataReturn) => {
      if(!dataReturn.success) {
        console.log(dataReturn)
        response.send(dataReturn);
        return;
      }

      const role = "USER_LOGGED";
      const payload = {
        user: {
          id: dataReturn.userFound.id,
          firstname: dataReturn.userFound.firstname,
          lastname: dataReturn.userFound.lastname,
          displayName: dataReturn.userFound.firstname + " " + dataReturn.userFound.lastname,
          username: dataReturn.userFound.username,
          role: role,
          email: dataReturn.userFound.email,
        },
        role: role
      };

      const token = jwt.sign(payload, GlobalStore.get('tokenSecretKey'), {
        expiresIn: GlobalStore.get('sessionDurationInMinutes') + 'm'
      });

      response.send({
        success: true,
        token: token,
        message: "Token created successfully"
      });
    }).catch(error => {
      response.send({
        success: false,
        error
      })
    })
  })

})

module.exports = router;
