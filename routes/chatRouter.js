const express = require('express');
const _ = require('lodash');
const Joi = require('joi');
const GlobalStore = require('../utils/GlobalStore');
const {features} = require('../permissions/ChatPermissions');
const {GET_CONTACTS_USER, GET_CURRENT_CONVERSATION, ADD_MESSAGE} = features;
const Auth = require('../utils/Auth');
const ChatSchema = require('../schemas/chatSchema');
const ChatManager = require('../lib/ChatManager');
const router = express.Router();

router.get("/get-contacts-user", Auth.verifyIfRoleHasPermission(GET_CONTACTS_USER), async function (request, response) {
    const config = GlobalStore.get("config");
    const {tokenData} = request;
    const userId = tokenData.user.id;
    const result = await ChatManager.getContactsByUserId(userId);
    if (result === {}) {
        response.send({
            success: true,
            contacts: "no-contacts"
        });
        return;
    }

    response.send({
        success: true,
        data: result
    })
});

router.post("/get-messages", Auth.verifyIfRoleHasPermission(GET_CURRENT_CONVERSATION), function (request, response) {
    const config = GlobalStore.get("config");
  
    Joi.validate(request.body, ChatSchema.getMessages, async function (error, data) {
      if (error) {
        console.log(error);
        return response.send({
          success: false,
          code: "missing-fields",
          error: error
        })
      }

    const {tokenData} = request;
    const userId = tokenData.user.id;
    
    const result = await ChatManager.getMessagesByConversationId(data.conversationId, userId)

    console.log(result);
    response.send({
        success: true,
        data: result,
    })

  })

})


router.post("/add-message", Auth.verifyIfRoleHasPermission(ADD_MESSAGE), function (request, response) {
  const config = GlobalStore.get("config");
  const {tokenData} = request;
  const userId = tokenData.user.id;

  Joi.validate(request.body, ChatSchema.addMessage, async function (error, data) {
    if (error) {
      console.log(error);
      return response.send({
        success: false,
        code: "missing-fields",
        error: error
      })
    }
    
    const result = await ChatManager.addMessage(data.conversationId, userId, data.content)

    response.send({
        success: true,
        data: result,
    })
  })
})

module.exports = router;
