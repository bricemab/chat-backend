const express = require('express');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const path = require('path');
const router = express.Router();
const GlobalStore = require('../utils/GlobalStore');
const Auth = require('../utils/Auth');
const Config = require("../config");
const Base64 = require("../utils/Base64");
const userSchema  = require("../schemas/userSchema");
const UserManager = require("../lib/userManager")


router.post("/add", function (request, response) {
    const config = GlobalStore.get("config");
  
    // Joi.validate(request.body, )
    Joi.validate(request.body, userSchema.addUser, async function (error, data) {
      if (error) {
        console.log(error);
        return response.send({
          success: false,
          code: "missing-fields",
          error: error
        })
      }
      UserManager.addUser(data).then(async (result) => {
        response.send(result);
      })
    })
  });

module.exports = router;
