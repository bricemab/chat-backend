const jwt = require('jsonwebtoken');
const config = require('../config');
const {USER_LOGGED, USER_ANONYMOUS, USER_ADMIN} = require('../permissions/Roles');
const {features: ChatFeatures, permissions: ChatPermissions} = require('../permissions/ChatPermissions');

const allFeatures = Object.assign({}, ChatFeatures);
const allPermissions = Object.assign({}, ChatPermissions);

const decodeToken = function (token) {
  return new Promise(function (resolve, reject) {
    if (token) {
      jwt.verify(token, config.tokenSecretKey, function (error, decodedToken) {
        if (error) {
          if (error.name === 'TokenExpiredError') {
            reject("token-expired");
          } else {
            reject("failed-to-authenticate-token");
          }
        } else {
          resolve(decodedToken)
        }
      });
    } else {
      reject("no-token-provided");
    }
  })
};

module.exports = {
  decodeToken,
  isUserLoggedIn: function (request, response, next) {
    decodeToken(request.headers['x-access-token']).then(function (tokenData) {
      request.tokenData = tokenData;

      if (tokenData.role === undefined || tokenData.role === USER_ANONYMOUS) {
        return response.status(401).send({success: false, message: "not-authorized"});
      } else {
        next();
      }
    }).catch(function (error) {
      return response.status(401).send({success: false, message: error});
    });
  },
  verifyIfRoleHasPermission: function (permissionTargeted) {
    return function (request, response, next) {

      const rawToken = request.headers['x-access-token'];
      const currentFeature = allFeatures[permissionTargeted];
      const currentFeaturePermissions = allPermissions[currentFeature];

      if (!rawToken) {
        if (currentFeaturePermissions.includes("USER_ANONYMOUS")) {
          return next();
        }
      }

      decodeToken(rawToken).then(function (tokenData) {
        request.tokenData = tokenData;
        if (currentFeature !== undefined) {
          const currentRole = tokenData.role || USER_ANONYMOUS;
          if (currentFeaturePermissions.includes(currentRole)) {
            next();
          } else {
            response.status(403).send({success: false, message: "not-authorized"});
          }
        } else {
          response.status(400).send({success: false, message: "feature-not-registered"});
        }
      }).catch(function (error) {
        response.status(401).send({success: false, message: error});
      });
    }
  },
};
