const base64 = require('base-64');
const utf8 = require('utf8');

module.exports = {
    encode: function ( text ) {
        return Buffer.from(text).toString('base64');
    },
    decode: function( encoded ){
        return utf8.decode(base64.decode(encoded));
    }
};
