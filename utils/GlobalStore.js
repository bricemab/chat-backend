const GlobalStore = {
    data: [],
    add: (key,item) => GlobalStore.data[key] = item,
    get: key => GlobalStore.data[key],
};

Object.freeze(GlobalStore);
module.exports = GlobalStore;
