const GlobalStore = require('../utils/GlobalStore');
const CryptoJs = require('crypto-js');

const UserManager = {
    async addUser(data) {
        const mysql = GlobalStore.get("dbConnection")

        const firstname = data.firstname;
        const lastname = data.lastname;
        const username = data.username;
        const password = UserManager.hashPassword(data.password);
        const email = data.email;
        const isEmailUsernameUnique = await UserManager.checkUsernameEmailIsUnique(username, email);
        return new Promise(async (resolve, reject) => {
            if(isEmailUsernameUnique) {
                const [userCreationResult, errorCreationResult] = await mysql.execute('INSERT INTO users (username, password, firstname, lastname, email) VALUES(:username, :password, :firstname, :lastname, :email)',
                {username, password, firstname, lastname, email})

                if(errorCreationResult) {
                    reject({
                        success: false,
                        error: JSON.stringify(errorCreationResult),
                        code: "creation-error"
                    });
                    return;
                } else {
                    const {insertId: userId} = userCreationResult;
                    resolve({
                        success: true,
                        data: {
                            userId,
                            firstname,
                            lastname,
                            email,
                            username
                        },
                    })
                }
            } else {
                resolve({
                    success: false,
                    code: "username-email-already-taken"
                })
                return;
            }

        })
    },
    async checkUsernameEmailIsUnique(username, email) {
        const mysql = GlobalStore.get("dbConnection")

        const [result] = await mysql.execute('SELECT * FROM `users` where email = ? or username = ?', [email, username]);
        if (result.length === 0) {
            return true;
        } else {
            return false;
        }
    },

    validateLoginAndPassword(login, password) {
        const mysql = GlobalStore.get("dbConnection");

        return new Promise(async (resolve, reject) => {
            if(!password && !login) {
                resolve({
                    success: false,
                    code: "fields-empty"
                });
                return;
            }

            const [userFound] = await mysql.execute('SELECT * FROM users where username = :login or email = :login', {login});
            if (userFound.length === 0) {
                resolve({
                    success: false,
                    code: "user-not-found"
                });
                return;
            }

            const hashedPassword = UserManager.hashPassword(password);
            if (userFound[0].password === hashedPassword) {
                resolve({
                    success: true,
                    userFound: userFound[0]
                })
            } else {
                resolve({
                    success: false,
                    code: "error-credentials"
                })
            }
            return;
        });
    },

    hashPassword(password) {
        return CryptoJs.SHA256(password).toString(CryptoJs.enc.Base64);
    },
}

module.exports = UserManager;
