const GlobalStore = require('../utils/GlobalStore');
const moment = require('moment');

const ChatManager = {
    async getContactsByUserId(userId) {
        const mysql = GlobalStore.get("dbConnection");

        userId = parseInt(userId) || 0;
        //get all contacts with table conversation, users
        const [result] = await mysql.execute("SELECT C.id as 'conversationId', C.title as 'conversationTitle', C.picture as 'conversationPicture', C.type as 'conversationType', U.id as 'id', U.username as 'username', U.lastname as 'lastname', U.firstname as 'firstname', U.picture as 'picture', U.mood as 'mood', if(strcmp(C.type, 'PRIVATE') = 0, U.username, C.title) as 'titleContact' FROM conversations as C LEFT JOIN user_conversation AS UC ON C.id = UC.conversation_id LEFT JOIN users as U on U.id = UC.user_id WHERE UC.conversation_id IN (SELECT UC.conversation_id FROM conversations LEFT JOIN user_conversation AS UC ON conversations.id = UC.conversation_id WHERE UC.user_id = :userId) and UC.user_id != :userId", {userId});
        if(result.length === 0) {
            return {};
        }

        for (const row of result) {
            const conversationId = row.conversationId;
            const [moreData] = await mysql.execute("SELECT COUNT(*) AS 'messagesNotRead', (SELECT is_seen FROM messages WHERE conversation_id = :conversationId ORDER BY id DESC LIMIT 1) AS `isLastMessageRead`, (SELECT `date` FROM messages WHERE conversation_id = :conversationId ORDER BY id DESC LIMIT 1) AS `lastDate`, (SELECT content FROM messages WHERE conversation_id = :conversationId ORDER BY id DESC LIMIT 1) AS `lastMessage`, (SELECT if (strcmp(user_id, :userId) = 0, 'me', 'not-me') FROM messages WHERE conversation_id = :conversationId ORDER BY id DESC LIMIT 1) AS `lastMessageOwner` FROM messages AS M WHERE M.id IN (SELECT M.id FROM chat.messages AS M WHERE M.conversation_id = :conversationId AND M.user_id != :userId) AND M.is_seen = 0 ORDER BY M.id DESC", {userId, conversationId});
            const newData = moreData[0];
            const lastDateMessage = moment(newData.lastDate);
            const today = moment();
            if (lastDateMessage.format('YYYY-MM-DD') === today.format('YYYY-MM-DD')) {
                row.lastDateMessage = lastDateMessage.format('HH:mm');
            } else {
                row.lastDateMessage = lastDateMessage.format('DD.MM.YYYY')
            }
            row.messagesNotRead = newData.messagesNotRead;
            row.isLastMessageRead = parseInt(newData.isLastMessageRead) !== 0;
            row.lastMessageOwner = newData.lastMessageOwner;
            row.lastMessage = newData.lastMessage;
        }

        return result;
    },

    async getMessagesByConversationId(convId, userId) {
        const mysql = GlobalStore.get("dbConnection");

        convId = parseInt(convId) || 0;
        userId = parseInt(userId) || 0;
        const [result] = await mysql.execute("SELECT *, if(strcmp(user_id, :userId) = 0, 'me', '') as 'messageOwner' FROM chat.messages where conversation_id = :convId order by date", {convId, userId});
        for (const row of result) {
            row.date = moment(row.date).format('YYYY-MM-DD HH:mm:ss');
            row.conversationId = row.conversation_id;
            delete row.conversation_id;
            row.userId = row.user_id;
            delete row.user_id;
            row.isSeen = row.is_seen == "1" ? true:false;
            delete row.is_seen;
        }

        return result;
    },

    async addMessage(convId, userId, content) {
        const mysql = GlobalStore.get("dbConnection");
        convId = parseInt(convId) || 0;
        userId = parseInt(userId) || 0;
        const [messageCreationResult, errorCreationResult] = await mysql.execute('INSERT INTO messages (conversation_id, user_id, content) VALUES(:convId, :userId, :content)',
        {convId, userId, content})

        if(errorCreationResult) {
            return {
                success: false,
                error: JSON.stringify(errorCreationResult),
                code: "add-message-error"
            };
        } else {
            const {insertId: messageId} = messageCreationResult;
            return {
                success: true,
                data: {
                    messages: await ChatManager.getMessagesByConversationId(convId, userId),
                    conversationId: convId
                },
            }
        }
    }
}


module.exports = ChatManager;
