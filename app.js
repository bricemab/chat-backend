const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const fileUpload = require('express-fileupload');
const apiRouter = require('./routes/genericRouter');
const usersRouter = require('./routes/usersRouter');
const chatRouter = require('./routes/chatRouter');
const helmet = require('helmet');
const cors = require('cors');

const rfs = require('rotating-file-stream');
const GlobalStore = require('./utils/GlobalStore');
const app = express();

// logger
app.use(morgan('dev'));
app.use(cookieParser());
app.use(helmet());
app.use(cors());
app.use(fileUpload());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/', apiRouter);
app.use('/chat', chatRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
// app.use(function (request, response, next) {
//   next(createError(404));
// });

// error handler
app.use(function (error, request, response, next) {
  // set locals, only providing error in development
  response.locals.message = error.message;
  response.locals.error = request.app.get('env') === 'development' ? error : {};

  response.status(error.status || 500);

  switch (error.status) {
    case 401:
      response.json({
        success: false,
        message: 'Not authorized'
      });
      break;
    case 404:
      response.json({
        success: false,
        message: 'This route does not exist'
      });
      break;
    default:
      console.log(error);
      response.json({
        success: false,
        message: 'An error has occurred while processing your request'
      });
      break;
  }
});

module.exports = app;

