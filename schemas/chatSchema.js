const Joi = require('joi');

const addMessage = Joi.object().keys({
  content: Joi.string().required(),
  conversationId: Joi.number().required(),
});

const getMessages = Joi.object().keys({
    conversationId: Joi.alternatives(
        Joi.string(),
        Joi.number()
    ).required()
})

module.exports = {
    getMessages,
    addMessage
};
