const Joi = require('joi');

const addUser = Joi.object().keys({
    lastname: Joi.string().required(),
    firstname: Joi.string().required(),
    email: Joi.string().required(),
    username: Joi.string().required(),
    password: Joi.string().required(),
});

module.exports = {
    addUser
};
