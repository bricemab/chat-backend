const Joi = require('joi');

const apiUser = Joi.object().keys({
  loginOrEmail: Joi.string().required(),
  password: Joi.string().required(),
});

module.exports = {
  apiUser
};