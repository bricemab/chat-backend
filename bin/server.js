#!/usr/bin/env node
const winston = require('winston');
const path = require('path');
const fs = require('fs');
const debug = require('debug')('web-service-icts:server');
const http = require('http');
const GlobalStore = require('../utils/GlobalStore');
const config = require('../config');
const mysql = require('mysql2/promise');
const cryptoJs = require('crypto-js');

/**
 * Create HTTP server.
 */

let server;
/**
 * Listen on provided port, on all network interfaces.
 */
setup().then(function () {
  const app = require('../app');

  const port = normalizePort(process.env.PORT || '5000');
  app.set('port', port);
  server = http.createServer(app);
  server.listen(port, "0.0.0.0");
  server.setTimeout(60*10*1000);
  server.on('error', onError);
  server.on('listening', onListening);
}).catch(function (e) {
  console.error(e);
});

async function setup() {
  try {
    const today = new Date();
    const logDirectory = path.join(__dirname, '../logs');
    const dateFileName = today.getFullYear() + "_" + (today.getMonth() + 1) + "_" + today.getUTCDate();
    fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

    function formatter(args) {
      const now = new Date();
      const nowToString = now.getFullYear() + "." + (now.getMonth() + 1) + "." + now.getUTCDate() + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
      return "[" + args.level.toLocaleUpperCase() + "] " + nowToString + ' ' + args.message;
    }

    const logger = new winston.createLogger({
      transports: [
        new winston.transports.Console({
          handleExceptions: true,
          level: 'info',
          json: false,
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.timestamp({
              format: 'DD.MM.YYYY HH:mm:ss'
            }),
            winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
          ),
        }),
        new (winston.transports.File)({
          name: 'info-file',
          filename: logDirectory + "/" + dateFileName + '_global_json.log',
          level: 'debug',
          json: true,
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.timestamp({
              format: 'DD.MM.YYYY HH:mm:ss'
            }),
            winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
          ),
        }),
        new (winston.transports.File)({
          name: 'info-file',
          filename: logDirectory + "/" + dateFileName + '_global.log',
          level: 'debug',
          json: false,
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.timestamp({
              format: 'DD.MM.YYYY HH:mm:ss'
            }),
            winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
          ),
        }),
        new (winston.transports.File)({
          name: 'warning-file',
          filename: logDirectory + "/" + dateFileName + '_warning.log',
          level: 'warn',
          json: false,
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.timestamp({
              format: 'DD.MM.YYYY HH:mm:ss'
            }),
            winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
          ),
        }),
        new (winston.transports.File)({
          name: 'error-file',
          filename: logDirectory + "/" + dateFileName + '_critical.log',
          level: 'error',
          json: false,
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.timestamp({
              format: 'DD.MM.YYYY HH:mm:ss'
            }),
            winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
          ),
        })
      ]
    });

    logger.info("Ecriture de logs dans " + logDirectory + "\\" + dateFileName + '****.log')
    GlobalStore.add('config', config);
    GlobalStore.add('tokenSecretKey', config.tokenSecretKey);
    GlobalStore.add('sessionDurationInMinutes', config.sessionDurationInMinutes);


    console.log("DATABASE CONNECTION CREATED");
    const pool = mysql.createPool({
      host: config.database.host,
      user: config.database.user,
      database: config.database.database,
      password: config.database.password,
      namedPlaceholders: true,
      waitForConnections: true,
      connectionLimit: 20,
      charset: "utf8_general_ci",
      queueLimit: 0
    });
    GlobalStore.add('dbConnection', pool);

    const id = 1
    const [result] = await pool.query('SELECT * FROM `conversations` where id = :id', {id});
    console.log(result, result.length, typeof result)
    console.log(result[0].title)

    // GlobalStore.add('serverMetas', moduleMetas);
  } catch (e) {
    console.error(e);
    throw e;
  }
}

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
  console.log("Listitening on port 5000")
}
