const {USER_ADMIN, USER_LOGGED} = require('./Roles');

const defaultModulePermissions = [
    USER_ADMIN,
    USER_LOGGED
];

module.exports = {
    features: {
        GET_CONTACTS_USER: "GET_CONTACTS_USER",
        GET_CURRENT_CONVERSATION: "GET_CURRENT_CONVERSATION",
        ADD_MESSAGE: "ADD_MESSAGE",
    },
    permissions: {
        GET_CONTACTS_USER: defaultModulePermissions,
        GET_CURRENT_CONVERSATION: defaultModulePermissions,
        ADD_MESSAGE: defaultModulePermissions
    }
};
